package com.whistemalo.codesandbox.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "users")
data class User(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @Embedded val name: UserName,
    @Embedded val location: UserLocation,
    @Embedded val picture: UserPicture,
)

@Dao
interface UserDao {
    @androidx.room.Query("SELECT * FROM users ORDER BY id DESC")
    fun getAllUsers(): LiveData<List<User>>

    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @androidx.room.Delete
    fun deleteUser(user: User)

    @androidx.room.Query("DELETE FROM users")
    fun deleteAllUsers()

    @androidx.room.Query("SELECT * FROM users WHERE id = :id")
    fun getUserById(id: Int): User
}


data class UserName(
    val title: String,
    val first: String,
    val last: String
)

data class UserPicture(
    val thumbnail: String
)

data class UserLocation (
    @Embedded
    val street: Street,
    val city: String,
    val state: String,
    val postcode: String
)

data class Street (
    val number: Int,
    val name: String
)

