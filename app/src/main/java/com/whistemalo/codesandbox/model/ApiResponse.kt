package com.whistemalo.codesandbox.model

class ApiResponse {
    val results: List<Results> = emptyList()
}

data class Results(
    val name: UserName?,
    val picture: UserPicture?,
    val location: UserLocation?
)