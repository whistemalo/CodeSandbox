package com.whistemalo.codesandbox.ui.landingPage


import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.valentinilk.shimmer.shimmer
import com.whistemalo.codesandbox.R
import com.whistemalo.codesandbox.model.User

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LandingPageUi(
    viewModel: LandingPageViewModel = hiltViewModel()
) {
    val users by viewModel.users.observeAsState(arrayListOf())
    val isLoading by viewModel.isLoading.observeAsState(false)

    Scaffold(
        topBar = {
            TopBar()
        },
        content = { padding -> Content(
            padding,
            viewModel = viewModel,
            users = users,
            isLoading = isLoading
            ) }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(
    viewModel: LandingPageViewModel = hiltViewModel()
) {
   TopAppBar(
       title = {
           Text(
               text = stringResource(id = R.string.app_name),
               fontWeight = FontWeight.Bold
           )
       },
      actions = {
        IconButton(onClick = { viewModel.addUser() }) {
            Icon(
                Icons.Filled.Add,
                "Add User"
            )
        }

      }
   )
}


@OptIn(ExperimentalCoilApi::class)
@Composable
fun Content(
    padding: PaddingValues = PaddingValues(16.dp),
    viewModel: LandingPageViewModel = hiltViewModel(),
    users: List<User>,
    isLoading: Boolean,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding)
    ) {
        Text(text = "Hello World")
        LazyColumn{
            var itemCount = users.size
            if (isLoading) itemCount++

            items(count = itemCount){
                var auxIndex = it
                if (isLoading) {
                    if (auxIndex == 0)
                        return@items LoadingCard()
                    auxIndex--
                }
                val user = users[auxIndex]

                Card(
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .padding(horizontal = 8.dp, vertical = 4.dp)
                        .fillMaxWidth()
                        .testTag("user_card")
                ){
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)){
                        Image(
                            modifier = Modifier.size(50.dp),
                            painter = rememberImagePainter(
                                data = user.picture.thumbnail,
                                builder = {
                                    placeholder(R.drawable.ic_launcher_foreground)
                                    error(R.drawable.ic_launcher_foreground)
                                }
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.FillHeight
                        )
                       Spacer(modifier =
                           Modifier
                               .size(8.dp)
                               .background(Color.Transparent)
                          )
                        Column(
                            modifier = Modifier
                                .weight(1f)
                                .fillMaxHeight()
                        ) {
                            Text(text = "${user.name.title} ${user.name.first} ${user.name.last}")
                            Text(user.location.city)
                        }
                        MySpacer()
                        IconButton(onClick = {
                            viewModel.deleteUser(user)
                        }) {
                            Icon(Icons.Filled.Delete, "Remove")
                        }
                    }
                }
            }


        }
    }
}

@Composable
fun LoadingCard() {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 4.dp)
            .fillMaxWidth()
            .testTag("loading_card")
    ){
        Row(modifier = Modifier.padding(8.dp)){
            ImageLoading()
            MySpacer()
            Column{
                MySpacer()
                Box(modifier = Modifier
                    .height(15.dp)
                    .fillMaxWidth()
                    .background(Color.Gray)
                )
                MySpacer()
                Box(
                    modifier = Modifier
                        .height(15.dp)
                        .fillMaxWidth()
                        .background(Color.Gray)
                )
            }
        }
    }
}
@Composable
fun MySpacer(size: Int = 8) = Spacer(modifier = Modifier.size(size.dp))

@Composable
fun ImageLoading() {
    Box(
       modifier = Modifier.shimmer()
    ) {
        Box(
            modifier = Modifier
                .size(50.dp)
                .background(Color.Gray)
        )
    }
}



