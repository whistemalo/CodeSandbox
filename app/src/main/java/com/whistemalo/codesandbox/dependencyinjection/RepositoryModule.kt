package com.whistemalo.codesandbox.dependencyinjection

import com.whistemalo.codesandbox.repository.UserRepository
import com.whistemalo.codesandbox.repository.UserRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun userRepository(repository: UserRepositoryImpl): UserRepository

}


