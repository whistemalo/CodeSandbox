package com.whistemalo.codesandbox

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CodeSandbox: Application();