package com.whistemalo.codesandbox.datasource

import androidx.room.Database
import androidx.room.RoomDatabase
import com.whistemalo.codesandbox.model.User
import com.whistemalo.codesandbox.model.UserDao

@Database(entities = [User::class], version = 1)
abstract class DbDataSource : RoomDatabase() {
    abstract fun userDao(): UserDao
}