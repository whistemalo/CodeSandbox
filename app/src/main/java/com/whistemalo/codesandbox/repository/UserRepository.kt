package com.whistemalo.codesandbox.repository

import androidx.lifecycle.LiveData
import com.whistemalo.codesandbox.datasource.RestDataSource
import com.whistemalo.codesandbox.model.*
import javax.inject.Inject

interface UserRepository {
    suspend fun getNewUser(): User
    suspend fun deleteUser(user: User)
    fun getAllUsers(): LiveData<List<User>>

    suspend fun deleteAllUsers()
    suspend fun getUserById(id: Int): User

}

class UserRepositoryImpl @Inject constructor(
    private val restDataSource: RestDataSource,
    private val userDao: UserDao
) : UserRepository {
    override suspend fun getNewUser(): User {
        val userName = restDataSource.getUserName()
        val userLocation = restDataSource.getUserLocation()
        val userPicture = restDataSource.getUserPicture()
        val user = User(
            name = UserName(
                title = userName.results[0].name?.title ?: "",
                first = userName.results[0].name?.first ?: "",
                last = userName.results[0].name?.last ?: ""
            ),
            location = UserLocation(
                street = userLocation.results[0].location?.street ?: Street(0, ""),
                city = userLocation.results[0].location?.city ?: "",
                state = userLocation.results[0].location?.state ?: "",
                postcode = userLocation.results[0].location?.postcode ?: ""
            ),
            picture = UserPicture(
                thumbnail = userPicture.results[0].picture?.thumbnail ?: ""
            )
        )
        userDao.insertUser(user)
        return user
    }

    override suspend fun deleteUser(user: User) = userDao.deleteUser(user)
    override fun getAllUsers(): LiveData<List<User>> = userDao.getAllUsers()
    override suspend fun deleteAllUsers() = userDao.deleteAllUsers()
    override suspend fun getUserById(id: Int): User = userDao.getUserById(id)
}